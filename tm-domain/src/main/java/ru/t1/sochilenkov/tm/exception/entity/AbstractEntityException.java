package ru.t1.sochilenkov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.exception.AbstractException;

public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
    }

    public AbstractEntityException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
