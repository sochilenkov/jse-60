package ru.t1.sochilenkov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sochilenkov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.sochilenkov.tm.api.service.dto.ISessionDtoService;
import ru.t1.sochilenkov.tm.dto.model.SessionDTO;
import ru.t1.sochilenkov.tm.exception.field.IdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.IndexIncorrectException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.exception.system.AuthenticationException;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<SessionDTO> result;
        result = repository.findAll(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO result;
        result = repository.findOneById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable SessionDTO result;
        result = repository.findOneByIndex(userId, index);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable SessionDTO session;
        session = repository.findOneByIndex(userId, index);
        if (session == null) return;
        repository.remove(userId, session);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@NotNull SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.removeById(session.getUserId(), session.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        repository.add(userId, session);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO add(@NotNull final SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.add(session.getUserId(), session);
        return session;
    }

}
