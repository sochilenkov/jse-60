package ru.t1.sochilenkov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sochilenkov.tm.api.repository.model.ITaskRepository;
import ru.t1.sochilenkov.tm.api.repository.model.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.model.ITaskService;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.model.User;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> result;
        result = repository.findAll(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task result;
        result = repository.findOneById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Task result;
        result = repository.findOneByIndex(userId, index);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Task task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        repository.remove(userId, task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @SuppressWarnings({"unchecked"})
    public List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> result;
        result = repository.findAll(userId, sort);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task;
        task = repository.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable final Task task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        task.setStatus(status);
        repository.update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<Task> result;
        result = repository.findAllByProjectId(userId, projectId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task = new Task();
        @Nullable final User user = userRepository.findOneById(userId);
        task.setUser(user);
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        repository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task;
        task = repository.findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task;
        task = repository.findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.update(task);
    }

}
