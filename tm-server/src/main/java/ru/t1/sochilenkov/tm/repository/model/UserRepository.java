package ru.t1.sochilenkov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.api.repository.model.IUserRepository;
import ru.t1.sochilenkov.tm.model.User;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull String login) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(query, getEntityClass()).setParameter("login", login).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull String email) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(query, getEntityClass()).setParameter("email", email).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}
